all: registry build message

registry:
	rmiregistry 2233 2>/dev/null &

build:
	javac cs3524/solutions/mud/*.java

server: registry build
	java cs3524.solutions.mud.MUDServer *.edg *.msg *.thg *.obj ${port} $(1) $(2) $(3)

client: registry build
	java cs3524.solutions.mud.MUDClient ${host} ${port} $(1) $(2) $(3)

message:
	@echo Please "make server" or "make client" depending on which one you need.
