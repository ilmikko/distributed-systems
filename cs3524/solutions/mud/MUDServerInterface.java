package cs3524.solutions.mud;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MUDServerInterface extends Remote {
	// GCS D
	public String joinUserServer(String name, String username) throws RemoteException;
	public String playerQuit(String username) throws RemoteException;

	// GCS C
	public String lookAround(String username) throws RemoteException;
	public String moveDirection(String username, String direction) throws RemoteException;
	public String pickUp(String username, String item) throws RemoteException;
	public String inventory(String username) throws RemoteException;

	// GCS B
	public String createUserServer(String name) throws RemoteException;
	public String listUserServers() throws RemoteException;

	// GCS A
	public String lookAt(String username, String item) throws RemoteException;
	public String listPlayers(String username) throws RemoteException;
	public String getMessages(String username) throws RemoteException;
	public void sendMessage(String username, String message) throws RemoteException;
}
