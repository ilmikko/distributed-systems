package cs3524.solutions.mud;

import java.util.*;

import java.rmi.RemoteException;

public class MUDEventListener implements Runnable {
	public MUDClientImpl client;
	public void run() {
		try{
			while(true) {
				client.getMessages();
				Thread.sleep(100);
			}
		}
		catch(RemoteException ex) {
			System.out.println("Remote exception in thread");
		}
		catch(InterruptedException ex) {
			System.out.println("Thread killed");
		}
	}

	public MUDEventListener(MUDClientImpl _client) {
		client = _client;
	}
}
