package cs3524.solutions.mud;

import java.net.InetAddress;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.RMISecurityManager;
import java.rmi.server.UnicastRemoteObject;

public class MUDServerImpl implements MUDServerInterface {
	private MUDConfig mudconfig;

	private HashMap<String, MUDUserServer> userServers = new HashMap<String, MUDUserServer>();
	private HashMap<String, MUDUserServer> userInServer = new HashMap<String, MUDUserServer>();

	private int maxUserServers = 5;

	private MUDUserServer getJoinedServer(String username) {
		return userInServer.get(username);
	}

	public String listUserServers() {
		String output = "";
		for (String server : userServers.keySet()) {
			output += "'" + server + "' ";
		}
		return output;
	}

	public String createUserServer(String name) {
		if (userServers.size() >= maxUserServers) {
			return "Too many servers! Please try again later.";
		}

		System.out.println("Creating a new user server '" + name + "'");
		userServers.put(name, new MUDUserServer(name, mudconfig));

		return "Server '" + name + "' was created successfully.";
	}

	public String joinUserServer(String name, String username) {
		userInServer.put(username, userServers.get(name));
		try{
			return getJoinedServer(username).playerJoin(username);
		}
		catch(Exception ex) {
			return "Error joining server " + name + ": " + ex;
		}
	}

	public String getMessages(String username) {
		return getJoinedServer(username).getMessages(username);
	}

	public void sendMessage(String username, String message) {
		getJoinedServer(username).sendMessage(username, message);
	}

	public String listPlayers(String username) {
		return getJoinedServer(username).listPlayers();
	}

	public String inventory(String username) {
		return getJoinedServer(username).inventory(username);
	}

	public String lookAround(String username) {
		return getJoinedServer(username).lookAround(username);
	}

	public String lookAt(String username, String item) {
		return getJoinedServer(username).lookAt(username, item);
	}

	public String moveDirection(String username, String direction) {
		return getJoinedServer(username).moveDirection(username, direction);
	}

	public String pickUp(String username, String item) {
		return getJoinedServer(username).pickUp(username, item);
	}

	public String playerQuit(String username) {
		String output = getJoinedServer(username).playerQuit(username);

		userServers.remove(username);

		return output;
	}

	private void register(int registryport, int serviceport) throws RemoteException {
		System.out.println("Registering server...");

		String hostname = "";
		try {
			hostname = (InetAddress.getLocalHost()).getCanonicalHostName();
		}
		catch(java.net.UnknownHostException e) {
			System.err.println("Cannot get host name: " + e.getMessage());
		}

		System.setProperty("java.security.policy", "mud.policy");
		System.setSecurityManager(new RMISecurityManager());

		MUDServerInterface mudstub = (MUDServerInterface)UnicastRemoteObject.exportObject(this, serviceport);
		
		String regURL = "rmi://" + hostname + ":" + registryport + "/MUDService";
		System.out.println("Register as " + regURL);
		try {
			Naming.rebind(regURL, mudstub);
		}
		catch(java.net.MalformedURLException e) {
			System.err.println("Malformed URL: " + e.getMessage());
		}
	}

	public MUDServerImpl(MUDConfig _mudconfig, int registryport, int serviceport) throws RemoteException {
		mudconfig = _mudconfig;
		register(registryport, serviceport);
	}
}
