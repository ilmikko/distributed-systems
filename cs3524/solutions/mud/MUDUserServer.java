package cs3524.solutions.mud;

import java.rmi.RemoteException;

import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;

// An instance of a user server that can be created from MUDServer.

public class MUDUserServer {
	private MUD mud;

	private String serverName;

	private int maxPlayers = 5;

	private List<String> players = new ArrayList<String>();
	private HashMap<String, String> playerLocations = new HashMap<String, String>();
	private HashMap<String, List<String>> playerInventories = new HashMap<String, List<String>>();
	private HashMap<String, List<String>> playerMessages = new HashMap<String, List<String>>();
	
	public String inventory(String username) {
		return "Your inventory contains: " + playerInventories.get(username);
	}

	public String lookAround(String username) {
		return mud.locationInfo(playerLocations.get(username));
	}

	public String lookAt(String username, String item) {
		String description = mud.lookAtObject(playerLocations.get(username), item);
		if (description != null) {
			return "You see" + description;
		} else {
			return "You cannot see " + item;
		}
	}

	public String moveDirection(String username, String direction) {
		String location = playerLocations.get(username);
		if (mud.getDirection(location, direction) != null) {
			playerLocations.put(username, mud.moveObject(location, direction, username));

			String view = mud.getDirection(location, direction);

			return "You travel " + view + "towards " + direction + ".\n" + lookAround(username);
		} else {
			return "You cannot travel " + direction;
		}
	}

	public String listPlayers() {
		String output = "Connected players:\n";
		for (String player : players) {
			output += player + "\n";
		}
		return output;
	}

	public String pickUp(String username, String item) {
		String location = playerLocations.get(username);
		if (mud.getThing(location, item) != null) {
			mud.delThing(location, item);
			playerInventories.get(username).add(item);

			return "You pick up " + item;
		} else {
			return "You cannot pick up " + item + ".";
		}
	}

	public void sendMessage(String username, String message) {
		System.out.println(message);
		for (List<String> messageList : playerMessages.values()) {
			messageList.add(message);
		}
	}

	public String getMessages(String username) {
		List<String> messageList = playerMessages.get(username);
		String messages = String.join("\n", messageList);
		messageList.clear();

		return messages;
	}

	public String playerJoin(String username) throws Exception {
		if (players.size() > maxPlayers) {
			throw new Exception("Too many players have joined. Please wait for a while.");
		}

		players.add(username);

		sendMessage(username, username + " has connected");
		String location = mud.startLocation();

		mud.addObject(location, username, "an adventurer on a journey to save the world");
		playerLocations.put(username, location);

		playerMessages.put(username, new ArrayList<String>());

		playerInventories.put(username, new ArrayList<String>());

		return "You have joined '" + serverName + "'.\nConnected players: " + players;
	}

	public String playerQuit(String username) {
		players.remove(username);

		sendMessage(username, username + " has disconnected");

		String location = playerLocations.get(username);
		mud.delThing(location, username);

		return "You have left '" + serverName + "'";
	}

	public MUDUserServer(String name, MUDConfig config) {
		serverName = name;
		mud = new MUD(config);
	}
}
