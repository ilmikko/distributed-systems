package cs3524.solutions.mud;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.rmi.RemoteException;

public class MUDMenu {
	private MUDClientImpl client;
	private String serverName;

	private String readInput(String message) {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.print(message);
		try {
			return in.readLine();
		}
		catch(IOException e) {
			return "";
		}
	}

	private void joinServer() {
		client.setUsername(readInput("Please enter your username: "));

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				try { client.quitServer(); }
				catch(RemoteException e) { }
			}
		}, "Shutdown-thread"));

		try {
			client.joinServer(serverName);
			game();
			client.quitServer();
		}
		catch(RemoteException ex) {
			System.out.println("You have been disconnected: " + ex);
		}
	}

	private void game() throws RemoteException {
		client.look("");

		while(true) {
			String cmdline = readInput("What will you do? ");

			if (cmdline == null) return;
			if (cmdline.length() == 0) continue;

			String[] command = cmdline.split("\\s+");
			switch(command[0].toLowerCase()) {
				case "exit": case "quit": case ":q":
					return;
				case "look": case "gaze": case "see":
					String item = "";
					for (int i = 1; i < command.length; i++) {
						switch(command[i]) {
							case "at":
							case "a": case "an": case "the":
							case "to": case "towards":
								continue;
						}
						item = command[i];
						break;
					}

					client.look(item);
					break;
				case "help":
					String[] commands = new String[]{
						"exit\t-\tExit the program",
						"help\t-\tShow this help screen",
						"look\t-\tLook at an object",
						"move\t-\tMove to a new direction",
						"pick (up)\t-\tPick up a thing",
						"say\t-\tSay something to other players",
						"shout\t-\tShout something to other players",
						"stuff\t-\tShow the inventory",
						"players\t-\tList the players",
					};
					System.out.println(String.join("\n", commands));
					break;
				case "move": case "walk": case "travel": case "run": case "go":
					String direction = "";
					for (int i = 1; i < command.length; i++) {
						switch(command[i]) {
							case "a": case "an":
							case "to": case "towards":
								continue;
						}
						direction = command[i];
						break;
					}

					if (direction == "") {
						System.out.println("Please specify a direction, e.g. '" + command[0] + " east'.");
						break;
					}

					client.move(direction);
					break;
				case "pick":
					String thing = "";
					for (int i = 1; i < command.length; i++) {
						switch(command[i]) {
							case "a": case "an":
							case "up":
								continue;
						}
						thing = command[i];
						break;
					}

					if (thing == "") {
						System.out.println("Please specify something to pick up, e.g. 'pick up coin'.");
						break;
					}

					client.pick(thing);
					break;
				case "players": case "online":
					client.playersOnline();
					break;
				case "say":
					client.sendMessage(command);
					break;
				case "shout":
					for (int i = 0; i < command.length; i++) {
						command[i] = command[i].toUpperCase();
					}
					client.sendMessage(command);
					break;
				case "stuff": case "inventory": case "inv":
					client.inventory();
					break;
				default:
					System.out.println("Sorry, I don't understand " + command[0]);
					break;
			}
		}
	}

	void createMenu() {
		System.out.println("Create a new MUD");

		String name = readInput("What is your MUD's name? ");

		try {
			client.createServer(name);
			serverName = name;
			joinServer();
		}
		catch(RemoteException ex) {
			System.out.println("Error creating server: " + ex);
		}
	}

	void joinMenu() {
		while(true) {
			System.out.println("Active mud servers:\n");
			try {
				client.listServers();
			}
			catch(RemoteException ex) {
				System.out.println("Error listing servers: " + ex);
			}
			System.out.println("\nType the name of the server you want to join.");
			System.out.println("Or type 'back' to go back.");

			String cmdline = readInput("> ");

			if (cmdline == null) return;
			if (cmdline.length() == 0) continue;

			String[] command = cmdline.split("\\s+");
			switch(command[0]) {
				case "0": case "back":
					return;
				default:
					serverName = command[0];
					joinServer();
					break;
			}
		}
	}

	void mainMenu() {
		while(true) {
			System.out.println("=========================");
			System.out.println(" Welcome to Mikko's MUD!");
			System.out.println("=========================\n");
			System.out.println("Main menu\n");
			System.out.println("1. Create new MUD");
			System.out.println("2. Join existing MUD");
			System.out.println("\n0. Exit");

			String cmdline = readInput("> ");

			if (cmdline == null) return;
			if (cmdline.length() == 0) continue;

			String[] command = cmdline.split("\\s+");
			switch(command[0]) {
				case "1": case "create":
					createMenu();
					break;
				case "2": case "join":
					joinMenu();
					break;
				case "0": case "exit":
					return;
				default:
					System.out.println("Command not understood: " + command[0]);
					break;
			}
		}
	}

	public MUDMenu(MUDClientImpl _client) {
		client = _client;
	}
}
