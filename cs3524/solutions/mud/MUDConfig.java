package cs3524.solutions.mud;

public class MUDConfig {
	public String edges;
	public String messages;
	public String things;
	public String objects;

	public MUDConfig(String _edges, String _messages, String _things, String _objects) {
		edges = _edges;
		messages = _messages;
		things = _things;
		objects = _objects;
	}
}
