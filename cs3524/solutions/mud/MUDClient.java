package cs3524.solutions.mud;

import java.net.InetAddress;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.rmi.RemoteException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.server.UnicastRemoteObject;

public class MUDClient {
	static String readInput(String message) {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.print(message);
		try {
			return in.readLine();
		}
		catch(IOException e) {
			return "";
		}
	}

	public static void main(String[] args) throws RemoteException {
		if (args.length < 2) {
			System.err.println("Usage: java MUDClient HOST PORT");
			return;
		}

		MUDClientImpl client = new MUDClientImpl(args[0], Integer.parseInt(args[1]));

		MUDMenu menu = new MUDMenu(client);

		menu.mainMenu();
	}
}
