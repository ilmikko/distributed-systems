package cs3524.solutions.mud;

import java.net.InetAddress;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.RMISecurityManager;
import java.rmi.server.UnicastRemoteObject;

public class MUDClientImpl {
	private String hostname;
	private int port;

	private Thread eventThread;

	private String username;

	private MUDServerInterface remote;

	private boolean connected;

	public void createServer(String name) throws RemoteException {
		System.out.println(remote.createUserServer(name));
	}

	public void listServers() throws RemoteException {
		System.out.println(remote.listUserServers());
	}

	public void joinServer(String name) throws RemoteException {
		if (connected) return;
		connected = true;
		System.out.println(remote.joinUserServer(name, username));
		eventThread = new Thread(new MUDEventListener(this));
		eventThread.start();
	}

	public void quitServer() throws RemoteException {
		if (!connected) return;
		connected = false;
		System.out.println(remote.playerQuit(username));
		eventThread.interrupt();
	}

	public void sendMessage(String[] message) throws RemoteException {
		message[0] = username + ": ";
		remote.sendMessage(username, String.join(" ", message));
	}

	public void getMessages() throws RemoteException {
		String messages = remote.getMessages(username);
		if (messages.equals("")) return;

		System.out.print("\n" + messages + "\nWhat will you do? ");
	}

	public void inventory() throws RemoteException {
		System.out.println(remote.inventory(username));
	}

	public void pick(String item) throws RemoteException {
		System.out.println(remote.pickUp(username, item));
	}

	public void move(String direction) throws RemoteException {
		System.out.println(remote.moveDirection(username, direction));
	}

	public void playersOnline() throws RemoteException {
		System.out.println(remote.listPlayers(username));
	}

	public void look(String item) throws RemoteException {
		if (item == null || item == "") {
			System.out.println(remote.lookAround(username));
		} else {
			System.out.println(remote.lookAt(username, item));
		}
	}

	public void setUsername(String _username) {
		username = _username;
		System.out.println("You are now known as " + username);
	}

	public void connect() {
		try {
			// Obtain the server handle from the RMI registry
			// listening at hostname:port.
			String regURL = "rmi://" + hostname + ":" + port + "/MUDService";
			System.out.println("Looking up " + regURL);
			remote = (MUDServerInterface)Naming.lookup(regURL);
		}
		catch (java.io.IOException e) {
			System.err.println("I/O error.");
			System.err.println(e.getMessage());
		}
		catch (java.rmi.NotBoundException e) {
			System.err.println("Server not bound.");
			System.err.println(e.getMessage());
		}
	}

	public MUDClientImpl(String _hostname, int _port) {
		hostname = _hostname;
		port = _port;

		System.setProperty("java.security.policy", "mud.policy");
		System.setSecurityManager(new SecurityManager());

		connect();
	}
}
