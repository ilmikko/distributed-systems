package cs3524.solutions.mud;

import java.rmi.RemoteException;

public class MUDServer {
	public static void main(String[] args) throws RemoteException {
		if (args.length != 5) {
		    System.err.println("Usage: MUDServer <edgesfile> <messagesfile> <thingsfile> <objectsfile> <port>");
		    return;
		}

		String edges = args[0];
		String messages = args[1];
		String things = args[2];
		String objects = args[3];

		MUDConfig config = new MUDConfig(edges, messages, things, objects);
		
		int port = Integer.parseInt(args[4]);
		
		MUDServerImpl s = new MUDServerImpl(config, port, 2244);
		System.out.println("Server now listening on " + port + "!");
	}
}
